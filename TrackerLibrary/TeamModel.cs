﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    /// <summary>
    /// reprezentuje jedną drużynę
    /// </summary>
    public class TeamModel
    {
        /// <summary>
        /// reprezentuje członków drużyny 
        /// </summary>
        public List<PersonModel> TeamMembers { get; set; } = new List<PersonModel>();


        /// <summary>
        /// reprezentuje  nazwę drużyny
        /// </summary>
        public string TeamName { get; set; }
    }
}
