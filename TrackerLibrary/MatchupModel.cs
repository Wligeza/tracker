﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    /// <summary>
    /// reprezentuje jeden mecz w turnieju
    /// </summary>
    public class MatchupModel
    {
        /// <summary>
        /// zbiór drużyn które brały idział w meczu
        /// </summary>
        public List<MatchupEntryModel> Entries { get; set; } = new List<MatchupEntryModel>();


        /// <summary>
        /// reprezentuje zwycięzcę tego meczu
        /// </summary>
        public TeamModel Winner { get; set; }


        /// <summary>
        /// reprezentuje runde której ten mecz jest częścią 
        /// </summary>
        public int MatchupRound { get; set; }
    }
}
