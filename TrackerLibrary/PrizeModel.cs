﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    /// <summary>
    /// reprezentuje wygraną 
    /// </summary>
    public class PrizeModel
    {
        /// <summary>
        /// reprezentuje numer miejsca, np na podium
        /// </summary>
        public int PlaceNumber { get; set; }


        /// <summary>
        /// reprezentuje nazwę miejsca, np pierwsze miejsce na podium to czempion
        /// </summary>
        public int PlaceName { get; set; }


        /// <summary>
        /// reprezentuje stawkę nagrody
        /// </summary>
        public decimal PrizeAmount { get; set; }


        /// <summary>
        /// reprezentuje procent nagrody, np 2 miejsce dostaje 25% całej nagrody 
        /// </summary>
        public double PrizePercentage { get; set; }
    }
}
