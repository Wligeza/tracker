﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary
{
    /// <summary>
    /// reprezentuje jedną osobę
    /// </summary>
    public class PersonModel
    {
        /// <summary>
        /// reprezentuje imię
        /// </summary>
        public string FirstName { get; set; }


        /// <summary>
        /// reprezentuje nazwisko
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// reprezentuje adres email
        /// </summary>
        public string EmailAddress { get; set; }


        /// <summary>
        /// reprezentuje nr tel
        /// </summary>
        public string Number { get; set; }
    }
}
